<?php
function mit3xxx_toolbar_admin() {

  
  
  // feature configurations.
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['settings']['mit3xxx_toolbar_theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#description' => t("The theme of the toolbar."),
    '#options' => array(
      'black-tie' => t('black-tie'),
      'blitzer' => t('blitzer'),
      'cupertino' => t('cupertino'),
      'dark-hive' => t('dark-hive'),
      'dot-luv' => t('dot-luv'),
      'eggplant' => t('eggplant'),
      'excite-bike' => t('excite-bike'),
      'flick' => t('flick'),
      'hot-sneaks' => t('hot-sneaks'),
      'humanity' => t('humanity'),
      'le-frog' => t('le-frog'),
      'mint-choc' => t('mint-choc'),
      'overcast' => t('overcast'),
      'pepper-grinder' => t('pepper-grinder'),
      'redmond' => t('redmond'),
      'smoothness' => t('smoothness'),
      'south-street' => t('south-street'),
      'start' => t('start'),
      'sunny' => t('sunny'),
      'swanky-purse' => t('swanky-purse'),
      'trontastic' => t('trontastic'),
      'ui-darkness' => t('ui-darkness'),
      'ui-lightness' => t('ui-lightness'),
      'vader' => t('vader')
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('mit3xxx_toolbar_theme', 'start')
  );

  $form['settings']['mit3xxx_toolbar_style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#description' => t("The Style of the toolbar."),
    '#options' => array(
      'cutter' => t('cutter'),
      'normal' => t('normal')  
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('mit3xxx_toolbar_style', 'cutter')
  );
  
  $form['settings']['mit3xxx_toolbar_position'] = array(
    '#type' => 'select',
    '#title' => t('Position'),
    '#description' => t("The position of the toolbar."),
    '#options' => array(
      'left' => t('left'),
      'right' => t('right'),
      'center_left' => t('center_left'),
      'center_right' => t('center_right')
  
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('mit3xxx_toolbar_position', 'left')
  );
  
  $form['settings']['mit3xxx_toolbar_distance'] = array(
    '#type' => 'textfield',
    '#title' => t('Distance from top'),
    '#description' => t("The distance from top."),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#default_value' => variable_get('mit3xxx_toolbar_distance', '100px')
  );
  
  $form['settings']['mit3xxx_toolbar_distance_from_position'] = array(
    '#type' => 'textfield',
    '#title' => t('Distance from position'),
    '#description' => t("The distance from position."),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#default_value' => variable_get('mit3xxx_toolbar_distance_from_position', '0px')
  );
  
  $form['settings']['mit3xxx_toolbar_show_back_to_top'] = array(
    '#type' => 'select',
    '#title' => t('Show JumpToTop button'),
    '#description' => t("Select JumpToTop visibility."),
    '#options' => array(
      'show' => t('show'),
      'hide' => t('hide')
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('mit3xxx_toolbar_show_back_to_top', 'show')
  );
  
  $form['settings']['mit3xxx_toolbar_show_back_to_bottom'] = array(
    '#type' => 'select',
    '#title' => t('Show JumpToBottom button'),
    '#description' => t("Select JumpToBottom visibility."),
    '#options' => array(
      'show' => t('show'),
      'hide' => t('hide')
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('mit3xxx_toolbar_show_back_to_bottom', 'show')
  );
  
  $form['settings']['mit3xxx_toolbar_show_twitter'] = array(
    '#type' => 'select',
    '#title' => t('Show twitter button'),
    '#description' => t("Select twitter visibility."),
    '#options' => array(
      'show' => t('show'),
      'hide' => t('hide')
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('mit3xxx_toolbar_show_twitter', 'show')
  );

  $form['settings']['mit3xxx_toolbar_show_facebook'] = array(
    '#type' => 'select',
    '#title' => t('Show facebook button'),
    '#description' => t("Select facebook visibility."),
    '#options' => array(
      'show' => t('show'),
      'hide' => t('hide')
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('mit3xxx_toolbar_show_facebook', 'show')
  );
  
  $form['settings']['mit3xxx_toolbar_show_bookmarks'] = array(
    '#type' => 'select',
    '#title' => t('Show bookmark button'),
    '#description' => t("Select bookmark visibility."),
    '#options' => array(
      'show' => t('show'),
      'hide' => t('hide')
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('mit3xxx_toolbar_show_bookmark', 'show')
  );

  $form['settings']['mit3xxx_toolbar_show_search'] = array(
    '#type' => 'select',
    '#title' => t('Show search button'),
    '#description' => t("Select search visibility."),
    '#options' => array(
      'show' => t('show'),
      'hide' => t('hide')
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('mit3xxx_toolbar_show_search', 'show')
  );

  $form['settings']['mit3xxx_toolbar_show_print'] = array(
    '#type' => 'select',
    '#title' => t('Show print button'),
    '#description' => t("Select print visibility."),
    '#options' => array(
      'show' => t('show'),
      'hide' => t('hide')
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('mit3xxx_toolbar_show_print', 'show')
  );
  
  $form['settings']['mit3xxx_toolbar_website'] = array(
    '#type' => 'textfield',
    '#title' => t('Website'),
    '#description' => t("The website of the toolbar."),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#default_value' => variable_get('mit3xxx_toolbar_website', 'http://')
  );

  $form['settings']['mit3xxx_toolbar_rss'] = array(
    '#type' => 'textfield',
    '#title' => t('RSS'),
    '#description' => t("The rss-feed of the toolbar."),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#default_value' => variable_get('mit3xxx_toolbar_rss', 'http://')
  );

  $form['settings']['mit3xxx_toolbar_search_website'] = array(
    '#type' => 'textfield',
    '#title' => t('Search website'),
    '#description' => t("Enter the url of your website. For example: http://www.mit3xxx.de/ "),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#default_value' => variable_get('mit3xxx_toolbar_search_website', 'http://')
  );
  
  $form['settings']['mit3xxx_toolbar_twitter_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter account'),
    '#description' => t("your twitter account"),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#default_value' => variable_get('mit3xxx_toolbar_twitter_account', '')
  );

  $form['settings']['mit3xxx_toolbar_twitter_text_begin'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter text begin'),
    '#description' => t("the begin of the twitter text"),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#default_value' => variable_get('mit3xxx_toolbar_twitter_text_begin', '')
  );

  $form['settings']['mit3xxx_toolbar_twitter_text_end'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter text end'),
    '#description' => t("the end of the twitter text"),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#default_value' => variable_get('mit3xxx_toolbar_twitter_text_end', '')
  );
  
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['account']['mit3xxx_toolbar_account'] = array(
    '#type' => 'textfield',
    '#title' => t('mit3xxx toolbar account number (optional)'),
    '#default_value' => variable_get('mit3xxx_toolbar_account', 'm3x-'),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => FALSE,
    '#description' => t('The mit3xxx toolbar account number. To retrieve the statistics (currenty not available). You can create an account on the following url: <a href="http://www.mit3xxx.de/">http://www.mit3xxx.de/</a>')
  );  
  
  
  // Seiten bestimmen auf der die toolbar angezeigt werden soll
  $form['pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );  
  $pages = variable_get('mit3xxx_toolbar_pages', '');
  $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
  $form['pages']['mit3xxx_toolbar_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Add to every page except the listed pages.'),
      '#default_value' => $pages,
      '#description' => $description,
      '#wysiwyg' => FALSE,
  );
  
  return system_settings_form($form);
}

function mit3xxx_toolbar_admin_validate($form, &$form_state) {

}